﻿using System.Diagnostics.CodeAnalysis;
using Discord;
using Discord.Net;
using Discord.WebSocket;
using Newtonsoft.Json;

namespace Shroombot;

internal class Program
{
#pragma warning disable CS8618
    internal static Config BotConfig;
    private DiscordSocketClient _client;
#pragma warning restore CS8618

    /// <summary>
    /// Entry point for Shroombot.
    /// </summary>
    /// <returns>The completed asynchronous task.</returns>
    private static Task Main() => new Program().Begin();

    /// <summary>
    /// Asynchronous entry point for Shroombot. Created as an instance so we can take advantage of non-static
    /// attributes.
    /// </summary>
    private async Task Begin()
    {
        this._client = new DiscordSocketClient();

        // Basically, this adds our Log method to the client's Log event
        // it's kind of like a forge event but this looks nicer if you know how it works
        this._client.Log += Log;
        this._client.Ready += Ready;
        this._client.SlashCommandExecuted += SlashCommands.SlashCommandHandler;

        // if you're hosting this bot, ask Jonathan for the token
        var token = Environment.GetEnvironmentVariable("SHROOMBOT_TOKEN");

        await this._client.LoginAsync(TokenType.Bot, token);
        await this._client.StartAsync();

        // Block this task until the program is closed.
        await Task.Delay(-1);
    }

    async Task Ready()
    {
        BotConfig = Config.Get();

        var mainServer = this._client.GetGuild(BotConfig.MainServerId);

        var faqEntryOption = new SlashCommandOptionBuilder()
            .WithName("faq-entry")
            .WithDescription("The FAQ entry for the bot to summarize.")
            .WithRequired(true)
            .WithType(ApplicationCommandOptionType.Integer);
        for (var i = 0; i < BotConfig.FaqEntries.Count; i++)
        {
            faqEntryOption.AddChoice(BotConfig.FaqEntries[i].name, i);
        }

        var faqCommand = new SlashCommandBuilder()
            .WithName("faq")
            .WithDescription("Get an FAQ number and assign someone the FAQ role.")
            .AddOption(faqEntryOption)
            .AddOption("user", ApplicationCommandOptionType.User, "The member to give the FAQ role to.",
                isRequired: false);

        try
        {
            // Now that we have our builder, we can call the CreateApplicationCommandAsync method to make our slash command.
            await mainServer.CreateApplicationCommandAsync(faqCommand.Build());
        }
        catch (HttpException exception)
        {
            // You can send this error somewhere or just print it to the console, for this example we're just going to print it.
            Console.WriteLine(JsonConvert.SerializeObject(exception.Errors, Formatting.Indented));
        }
    }

    Task Log(LogMessage msg)
    {
        Console.WriteLine(msg.ToString());
        return Task.CompletedTask;
    }
}
