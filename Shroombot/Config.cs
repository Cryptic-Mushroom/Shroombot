﻿using YamlDotNet.Serialization.NamingConventions;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable MemberCanBePrivate.Global

namespace Shroombot;

internal class Config
{
#pragma warning disable CS8618 CS0649
    public ConfigMain main;
    public ConfigDev dev;
    public List<FAQEntry> faq;
#pragma warning restore CS8618 CS0649

    public ulong MainServerId => main.id;
    public ulong MainServerRole => main.role;
    public ulong DevServerId => dev.id;
    public List<FAQEntry> FaqEntries => faq;

    internal static Config Get()
    {
        return new YamlDotNet.Serialization.DeserializerBuilder()
            .WithNamingConvention(CamelCaseNamingConvention.Instance)
            .Build()
            .Deserialize<Config>(File.ReadAllText("config.yaml"));
    }

#pragma warning disable CS0649 CS8618
    public class ConfigMain
    {
        public ulong id;
        public ulong role;
    }

    public class ConfigDev
    {
        public ulong id;
    }

    public class FAQEntry
    {
        public string name;
        public string desc;
    }
#pragma warning restore CS0649 CS8618
}
