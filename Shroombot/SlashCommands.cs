﻿using Discord;
using Discord.WebSocket;

namespace Shroombot;

internal class SlashCommands
{
    internal static async Task SlashCommandHandler(SocketSlashCommand command)
    {
        long entry = -1;
        SocketGuildUser victim = null;
        if (command.User is not SocketGuildUser || command.Channel is not IGuildChannel)
        {
            await command.RespondAsync("Something went wrong processing that command. Bully Jonathan to see if something went wrong.", ephemeral: true);
            return;
        }

        var caller = (SocketGuildUser) command.User;

        foreach (var option in command.Data.Options.ToList())
        {
            switch (option.Name)
            {
                case "faq-entry":
                    entry = (long) option.Value;
                    break;
                case "user":
                    victim = (SocketGuildUser) option.Value;
                    break;
            }
        }

        string message = Program.BotConfig.FaqEntries[(int) entry].desc;
        if (victim != null)
        {
            if (!caller.GetPermissions(command.Channel as IGuildChannel).ManageRoles)
            {
                await command.RespondAsync("You don't have permission to bully someone with the FAQ role.");
                return;
            }

            message += $" Enjoy the role, {victim.Mention}.";
            await victim.AddRoleAsync(victim.Guild.GetRole(946488169216495657));
        }

        await command.RespondAsync(message);
    }
}
